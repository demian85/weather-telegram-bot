'use strict';

global.config = Object.assign(
    require('./config-default'),
    require('./config')
);

const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(config.telegramBotToken, {polling: true});
const OpenWeatherMapApi = require('./lib/openweathermap.js');

bot.onText(/\/clima(?:@[^ ]+)?(.*)/, function(msg, match) {
    const targetId = msg.chat.id ? msg.chat.id : msg.from.id;
    const city = match[1] ? match[1].trim() : '';
    const weatherApi = new OpenWeatherMapApi();

    weatherApi.getWeather(city)
        .then((data) => {
            bot.sendPhoto(targetId, data.icon, {caption: data.title}).then(() => {
                bot.sendMessage(targetId, data.details + '\n\n' + data.description);
            });
        })
        .catch((err) => {
            bot.sendMessage(targetId, err.message);
            console.log(err.stack);
        });
});
