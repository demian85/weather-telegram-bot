'use strict';

const moment = require('moment');

moment.locale('es');

class OpenWeatherMapApi {

    getCurrent(city) {
        return this._apiRequest('weather', city).then(data => {
            return this._parseItem(data);
        });
    }

    getForecast(city) {
        return this._apiRequest('forecast', city).then(data => {
            return data.list.map(item => {
                return this._parseItem(item);
            });
        });
    }

    getWeather(city) {
        return Promise.all([
            this.getCurrent(city),
            this.getForecast(city)
        ]).then((values) => {
            const current = values[0];
            const forecast = values[1];
            var desc = [
                this._getWeatherConditionAsFriendlyString(current) + ' ' + this._getWeatherSensationAsFriendlyString(current)
            ];
            var forecastChange = this._findForecastChanges(current, forecast);
            if (forecastChange) {
                desc.push('__________\nA las ' + moment(forecastChange.dt*1000).format('ha') + ' cambian las condiciones:');
                desc.push(this._getWeatherConditionAsFriendlyString(forecastChange));
                desc.push(this._getWeatherDetails(forecastChange));
            } else {
                desc.push('No hay cambios significativos para el resto del día.')
            }
            return {
                title: 'El clima en ' + current.city,
                details: this._getWeatherDetails(current),
                description: desc.join('\n'),
                icon: this._getWeatherConditionIcon(current.weather)
            };
        });
    }

    _apiRequest(method, city) {
        city = city || config.defaultCity;
        return new Promise((resolve, reject) => {
            const request = require('request');
            const url = 'http://api.openweathermap.org/data/2.5/' + method + '?q=' + encodeURIComponent(city) + '&units=metric&&appid=' + config.openWeatherMapAppId;
            request(url, (err, response, body) => {
                if (err || response.statusCode !== 200) {
                    return reject(err || new Error('Invalid response!'));
                }
                var res = JSON.parse(body);
                if (res.cod != 200) {
                    reject(new Error(res.message));
                } else {
                    resolve(res);
                }
            });
        });
    }

    _findForecastChanges(currentWeather, forecast) {
        var conditionId = currentWeather.weather[0].id;
        var firstChange = forecast.find((item) => {
            // detect change in weather condition range
            var currentConditionIdBase = conditionId - (conditionId % 100);
            var itemCondIdBase = item.weather[0].id - (item.weather[0].id % 100);
            var isSameDay = new Date().getDate() === new Date(item.dt*1000).getDate();
            var conditionDiff = Math.abs(currentConditionIdBase - itemCondIdBase) >= 100;
            var tempDiff = Math.abs(item.temp - currentWeather.temp) >= 5;
            var windDiff = Math.abs(item.wind - currentWeather.wind) >= 10;
            return isSameDay && (conditionDiff || tempDiff || windDiff);
        });
        return firstChange || null;
    }

    _parseItem(data) {
        return {
            dt: data.dt || null,
            city: data.city ? data.city.name : data.name,
            weather: data.weather,
            temp: data.main.temp,
            humidity: data.main.humidity,
            pressure: data.main.pressure,
            wind: data.wind
        };
    }

    _getWeatherDetails(weather) {
        const cardinalPoint = ['N', 'NE', 'E', 'SE', 'S', 'SO', 'O', 'NO', 'N'][Math.round((weather.wind.deg%360)/45)];
        return [
            'Temperatura: ' + Math.round(weather.temp) + ' °C',
            'Humedad: ' + Math.round(weather.humidity) + '%',
            'Viento: ' + cardinalPoint + ' a ' + Math.round(weather.wind.speed) + ' km/h',
            'Presión: ' + Math.round(weather.pressure) + ' hPa'
        ].join('\n');
    }

    _getWeatherConditionAsFriendlyString(weather) {
        var cond = weather.weather[0];
        if (cond.id >= 200 && cond.id < 300) { // Group 2xx: Thunderstorm
            return 'Tormentas! Rayos y sentellas!';
        } else if (cond.id >= 300 && cond.id < 400) { // Group 3xx: Drizzle
            return 'Llovizna.'
        } else if (cond.id >= 500 && cond.id < 600) { // Group 5xx: Rain
            return 'Llueve' + (cond.id >= 502 ? ' como la puta madre!' : '!');
        } else if (cond.id === 800) { // Group 800: Clear
            return 'Despejado.';
        } else if (cond.id > 800 && cond.id < 900) { // Group 80x: Clouds
            return 'Parcialmente nublado.'
        }
        return 'Algo raro está pasando! Mejor mirá por la ventana...';
    }

    _getWeatherSensationAsFriendlyString(weather) {
        const humidityRatio = weather.humidity >= 80 ? weather.humidity * 0.05 : 1;
        const sensation = Math.round(weather.temp + humidityRatio);
        if (sensation < 15) {
            return 'Abrigate o te vas a cagar de frío!';
        } else if (sensation >= 15 && sensation <= 20) {
            return 'Está fresco. Llevá abrigo.';
        } else if (sensation > 20 && sensation <= 25) {
            return 'Agradable, está para remerita.';
        } else if (sensation > 25 && sensation < 30) {
            return 'Está para zunga!';
        }
        return 'Te vas a derretir!';
    }

    _getWeatherConditionIcon(weather) {
        var cond = weather[0];
        return './icons/' + cond.icon + '.png';
    }
}

module.exports = OpenWeatherMapApi;
