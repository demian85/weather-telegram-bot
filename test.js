var OpenWeatherMapApi = require('./lib/openweathermap.js');
var weatherApi = new OpenWeatherMapApi();

weatherApi.getWeather('Colon,AR')
.then((data) => {
    console.log(data);
})
.catch((err) => {
    console.log(err.stack);
});
