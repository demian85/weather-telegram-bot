# Telegram bot using OpenWeatherMap API #

Only hardcoded spanish for now!

## Available commands ##

* /clima [<ciudad, código de país>]

## Setup ##

* Create a bot: https://core.telegram.org/bots
* Get an API key for OpenWeatherMap: http://openweathermap.org/api
* Create a config.json file in the root directory with the following structure:

```
{
    "telegramBotToken": "...",
    "openWeatherMapAppId": "...",
    "defaultCity": "Buenos Aires, AR"
}

```
